var app=angular.module("nestedcontroller",[]);

app.controller("ctrl1",ctrl1);
app.controller("ctrl2",ctrl2);

function ctrl1()
{
    this.testProp="from ctrl1";
}

function ctrl2()
{
    this.testProp="from ctrl2";
}