

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/ViewStudent")
public class ViewStudent extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("<a href='index.html'>Add New Student</a>");
		out.println("<h1>Students List</h1>");
		
		List<Stud> list=StudDao.getAllStudents();
		
		out.print("<table border='1' width='100%'");
		out.print("<tr><th>Id</th><th>Name</th><th>m1</th><th>m2</th><th>Email</th><th>Grade</th><th>Edit</th><th>Delete</th></tr>");
		for(Stud s:list){
			out.print("<tr><td>"+s.getid()+"</td><td>"+s.getname()+"</td><td>"+s.getm1()+"</td><td>"+s.getm2()+"</td><td>"+s.getemail()+"</td><td>"+s.getgrade()+"</td><td><a href='EditStudent?id="+s.getid()+"'>edit</a></td><td><a href='DeleteStudent?id="+s.getid()+"'>delete</a></td></tr>");
		}
		out.print("</table>");
		
		out.close();
	}
	
}
