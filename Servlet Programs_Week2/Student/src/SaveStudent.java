

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/SaveStudent")
public class SaveStudent extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		
		String name=request.getParameter("name");
		int m1=Integer.parseInt(request.getParameter("m1"));
		int m2=Integer.parseInt(request.getParameter("m2"));
	
		String email=request.getParameter("email");
		String grade=request.getParameter("grade");
		
		Stud s=new Stud();
		s.setname(name);
		s.setm1(m1);
		s.setm2(m2);
		s.setemail(email);
		s.setgrade(grade);
		
		int status=StudDao.save(s);
		
		if(status>0){
			out.print("<p>Record saved successfully!</p>");
			request.getRequestDispatcher("index.html").include(request, response);
		}else{
			
			out.println("Sorry! unable to save record");
		}
		
		out.close();
	}

	
}
