

public class Stud {
	
	private int id,m1,m2;
	private String name,email,grade;
	
	public int getid()
	{
		return id;
	}
	
	public void setid(int id)
	{
		this.id=id;
	}
	public int getm1()
	{
		return m1;
	}
	public void setm1(int m1)
	{
		this.m1=m1;
	}
	public int getm2()
	{
		return m2;
	}
	public void setm2(int m2)
	{
		this.m2=m2;
	}
	public String getname()
	{
		return name;
	}
	public void setname(String name)
	{
		this.name=name;
	}
	
	public String getemail()
	{
		return email;
	}
	public void setemail(String email)
	{
		this.email=email;
	}
	
	public String getgrade()
	{
		return grade;
	}
	public void setgrade(String grade)
	{
		this.grade=grade;
	}
	
}
