

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/EditStudent")
public class EditStudent extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("<h1>Update Student</h1>");
		String sid=request.getParameter("id");
		int id=Integer.parseInt(sid);
		
		Stud s=StudDao.getStudentById(id);
		
		out.print("<form action='EditStudent2' method='post'>");
		out.print("<table>");
		out.print("<tr><td></td><td><input type='hidden' name='id' value='"+s.getid()+"'/></td></tr>");
		out.print("<tr><td>Name:</td><td><input type='text' name='name' value='"+s.getname()+"'/></td></tr>");
		out.print("<tr><td>Password:</td><td><input type='text' name='m1' value='"+s.getm1()+"'/></td></tr>");
		out.print("<tr><td>Password:</td><td><input type='text' name='m2' value='"+s.getm2()+"'/></td></tr>");
		out.print("<tr><td>Email:</td><td><input type='email' name='email' value='"+s.getemail()+"'/></td></tr>");
		out.print("<tr><td>Grade:</td><td>");
		out.print("<select name='Grade' style='width:150px'>");
		out.print("<option>A</option>");
		out.print("<option>B</option>");
		out.print("<option>C</option>");
		out.print("<option>Other</option>");
		out.print("</select>");
		out.print("</td></tr>");
		out.print("<tr><td colspan='2'><input type='submit' value='Edit &amp; Save '/></td></tr>");
		out.print("</table>");
		out.print("</form>");
		
		out.close();
	}
	
}
