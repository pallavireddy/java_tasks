

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StudDao {
	public static Connection getConnection(){
		Connection con=null;
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("Connected");
			
		con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","pallavi906");
		System.out.println("After con");
		}catch(Exception e){System.out.println(e);}
		return con;
	}
	public static int save(Stud s){
		int status=0;
		try{
			Connection con=StudDao.getConnection();
			PreparedStatement ps=con.prepareStatement("insert into student(id,name,m1,m2,email,grade) values (sid1.nextval,?,?,?,?,?)");
			ps.setInt(1,s.getid());
			ps.setString(2,s.getname());
			ps.setInt(3,s.getm1());
			ps.setInt(4,s.getm2());
			ps.setString(5,s.getemail());
			ps.setString(6,s.getgrade());
			
			status=ps.executeUpdate();
			
			con.close();
		}catch(Exception ex){ex.printStackTrace();}
		
		return status;
	}
	public static int update(Stud s){
		int status=0;
		try{
			Connection con=StudDao.getConnection();
			PreparedStatement ps=con.prepareStatement("update student set name=?,m1=?,m2=?,email=?, grade=? where id=?");
			ps.setInt(1,s.getid());
			ps.setString(2,s.getname());
			ps.setInt(3,s.getm1());
			ps.setInt(4,s.getm2());
			ps.setString(5,s.getemail());
			ps.setString(6,s.getgrade());
			
			status=ps.executeUpdate();
			
			con.close();
		}catch(Exception ex){ex.printStackTrace();}
		
		return status;
	}
	public static int delete(int id){
		int status=0;
		try{
			Connection con=StudDao.getConnection();
			PreparedStatement ps=con.prepareStatement("delete from student where id=?");
			ps.setInt(1,id);
			status=ps.executeUpdate();
			
			con.close();
		}catch(Exception e){e.printStackTrace();}
		
		return status;
	}
	
	public static Stud getStudentById(int id){
		Stud s=new Stud();
		
		try{
			Connection con=StudDao.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from student where id=?");
			ps.setInt(1,id);
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				s.setid(rs.getInt(1));
				s.setname(rs.getString(2));
				s.setm1(rs.getInt(3));
				s.setm2(rs.getInt(4));
				s.setemail(rs.getString(5));
				s.setgrade(rs.getString(6));
			}
			con.close();
		}catch(Exception ex){ex.printStackTrace();}
		
		return s;
	}
	public static List<Stud> getAllStudents(){
		List<Stud> list=new ArrayList<Stud>();
		
		try{
			Connection con=StudDao.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from student");
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				Stud s=new Stud();
				s.setid(rs.getInt(1));
				s.setname(rs.getString(2));
				s.setm1(rs.getInt(3));
				s.setm2(rs.getInt(4));
				s.setemail(rs.getString(5));
				s.setgrade(rs.getString(6));
				list.add(s);
			}
			con.close();
		}catch(Exception e){e.printStackTrace();}
		
		return list;
	}
}
